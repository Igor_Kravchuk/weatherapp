import UIKit
//MARK: - ENUMS

class HourlyWeatherCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var hourLabel: UILabel?
    @IBOutlet weak var temperatureLabel: UILabel?
    @IBOutlet weak var weatherIconImage: UIImageView?
    
    //MARK: - VARS
    
    //MARK: - LETS
    
    //MARK: - Functions
    func config(with object: Hourly){
        self.addHoursLabelText(with: object)
        self.addTemperatureLabelText(with: object)
        self.addIconImage(with: object)
    }
    
    func addHoursLabelText(with object: Hourly) {
        if let date = object.dt {
            let date = Date(timeIntervalSince1970: TimeInterval(date))
            let formatter = DateFormatter()
            formatter.dateFormat = "HH"
            let hourString = formatter.string(from: date)
            self.hourLabel?.text = hourString
        }
    }
    
    func addTemperatureLabelText(with object: Hourly) {
        if let temperature = object.temp {
            self.temperatureLabel?.text = "\(temperature)°"
        }
    }
    
    func addIconImage(with object: Hourly) {
        if let icon = object.weather?[0].icon {
            self.weatherIconImage?.image = UIImage(named: icon)
        }
    }
    
}
