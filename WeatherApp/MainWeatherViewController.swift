import UIKit

//MARK: - ENUM
class MainWeatherViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var cityNameLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var currentTemperature: UILabel?
    @IBOutlet weak var minMaxTemperatureLabel: UILabel?
    @IBOutlet weak var sunriseLabel: UILabel?
    @IBOutlet weak var sunsetLabel: UILabel?
    @IBOutlet weak var humidityLabel: UILabel?
    @IBOutlet weak var windLabel: UILabel?
    @IBOutlet weak var feelsLikeLabel: UILabel?
    @IBOutlet weak var pressureLabel: UILabel?
    @IBOutlet weak var visibilityLabel: UILabel?
    @IBOutlet weak var uviLabel: UILabel?
    @IBOutlet weak var hourlyWeatherCollectionView: UICollectionView?
    @IBOutlet weak var hourlyWeatherCollectionViewBackground: UIView?
    @IBOutlet weak var weekWeatherTableView: UITableView?
    @IBOutlet weak var dailyWeatherTableViewBackground: UIView?
    @IBOutlet weak var weatherInformationLabel: UILabel?
    @IBOutlet weak var backgroundImage: UIImageView?
    @IBOutlet weak var cityNameBackground: UIView?
    @IBOutlet weak var scrollViewBackground: UIView?
    @IBOutlet weak var pageControllerBackground: UIView?
    
    //MARK: - VARS
    var mainWeatherViewModel: MainWeatherViewModel?
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    var loadingView: UIView = UIView()
    
    //MARK: - LETS
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showActivityIndicator()
        self.mainWeatherViewModel = MainWeatherViewModel()
        self.bindOutlets()
        self.mainWeatherViewModel?.startUpdateLocation()
        self.mainWeatherViewModel?.loadWeather()
        self.backgroundImage?.addParalaxEffect()
    }
    
    //MARK: - IBActions
    
    //MARK: - Flow functions
    private func bindOutlets() {
        self.mainWeatherViewModel?.weather.bind({ [weak self] (weather) in
            DispatchQueue.main.async {
                guard weather != nil else { return }
                self?.hideActivityIndicator()
                self?.showAllMainBackgrounds()
                self?.cityNameLabel?.text = self?.mainWeatherViewModel?.writeCityName(value: weather?.timezone ?? "")
                self?.descriptionLabel?.text = weather?.current?.weather?[0].description
                if let currentTemperature = weather?.current?.temp {
                    self?.currentTemperature?.text = "\(currentTemperature)°"
                }
                self?.writeMinMaxTemperatureLabel()
                self?.hourlyWeatherCollectionView?.reloadData()
                self?.weekWeatherTableView?.reloadData()
                self?.writeMinMaxTodayWeather()
                self?.writeAccurateInfoView()
            }
        })
    }
    
    func showActivityIndicator() {
        self.loadingView.frame = CGRect(x: 0.0, y: 0.0, width: 100.0, height: 100.0)
        self.loadingView.center = self.view.center
        self.loadingView.backgroundColor = .clear
        self.spinner = UIActivityIndicatorView(style: .large)
        self.spinner.color = .cyan
        self.spinner.frame = self.loadingView.frame
        self.spinner.center = CGPoint(x:self.loadingView.bounds.size.width / 2, y:self.loadingView.bounds.size.height / 2)
        self.loadingView.addSubview(self.spinner)
        self.view.addSubview(self.loadingView)
        self.spinner.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.spinner.stopAnimating()
        self.loadingView.removeFromSuperview()
    }
    
    func showAllMainBackgrounds() {
        self.cityNameBackground?.isHidden = false
        self.scrollViewBackground?.isHidden = false
        self.pageControllerBackground?.isHidden = false
    }
    
    func hideAllMainBackgrounds() {
        self.cityNameBackground?.isHidden = true
        self.scrollViewBackground?.isHidden = true
        self.pageControllerBackground?.isHidden = true
    }
    
    func writeMinMaxTemperatureLabel() {
        if let todayTemperature = self.mainWeatherViewModel?.weather.value?.daily?[0].temp {
            if let minTemp = todayTemperature.min,
               let maxTemp = todayTemperature.max {
                let fistStringLocalizedPart = "Max. ".localized
                let secondStringLocalizedPart = "°; Min. ".localized
                let thirdStringLocalized = "°".localized
                let localizedMinMaxTemperatureString = fistStringLocalizedPart + "\(maxTemp)" + secondStringLocalizedPart + "\(minTemp)" + thirdStringLocalized
                self.minMaxTemperatureLabel?.text = localizedMinMaxTemperatureString
            }
        }
    }
    
    func writeMinMaxTodayWeather() {
        if let todayWeather = self.mainWeatherViewModel?.weather.value?.daily?[0],
           let weatherDescription = todayWeather.weather?[0].description,
           let minTemperature = todayWeather.temp?.min,
           let maxTemperature = todayWeather.temp?.max {
            let firstStringLocalizedPart = "Today will be ".localized
            let secondStringLocalizedPart = ". The highest temperature will be ".localized
            let thirdStringLocalizedPart = "°, the lowest temperature will be ".localized
            let fourthStringLocalizedPart = "°.".localized
            self.weatherInformationLabel?.text = firstStringLocalizedPart + "\(weatherDescription)" + secondStringLocalizedPart + "\(maxTemperature)" + thirdStringLocalizedPart + "\(minTemperature)" + fourthStringLocalizedPart
        }
    }
    
    func writeSunriswInfo(currentWeather: Current) {
        if let sunrise = currentWeather.sunrise {
            let sunriseDate = Date(timeIntervalSince1970: TimeInterval(sunrise))
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: sunriseDate)
            self.sunriseLabel?.text = dateString
        }
    }
    
    func writeSunsetInfo(currentWeather: Current) {
        if let sunset = currentWeather.sunset {
            let sunsetDate = Date(timeIntervalSince1970: TimeInterval(sunset))
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let dateString = formatter.string(from: sunsetDate)
            self.sunsetLabel?.text = dateString
        }
    }
    
    func writeHumidityInfo(currentWeather: Current) {
        if let humidity = currentWeather.humidity {
            self.humidityLabel?.text = "\(humidity) %"
        }
    }
    
    func writeWindSpeedInfo(currentWeather: Current) {
        if let windSpeed = currentWeather.wind_speed {
            let localizedString = "m/sec".localized
            self.windLabel?.text = "\(windSpeed) " + localizedString
        }
    }
    
    func writeFeelsLikeInfo(currentWeather: Current) {
        if let feelsLike = currentWeather.feels_like {
            self.feelsLikeLabel?.text = "\(feelsLike)°"
        }
    }
    
    func writePressureInfo(currentWeather: Current) {
        if let pressure = currentWeather.pressure {
            let localizedString = "hPa".localized
            self.pressureLabel?.text = "\(pressure) " + localizedString
        }
    }
    
    func writeVisibilityInfo(currentWeather: Current) {
        if let visibility = currentWeather.visibility {
            let localizedString = "km".localized
            self.visibilityLabel?.text = "\(Double(visibility) / 1000) " + localizedString
        }
    }
    
    func writeUviInfo(currentWeather: Current) {
        if let uvi = currentWeather.uvi {
            self.uviLabel?.text = "\(uvi)"
        }
    }
    
    func writeAccurateInfoView() {
        if let currentWeather = self.mainWeatherViewModel?.weather.value?.current {
            self.writeSunriswInfo(currentWeather: currentWeather)
            self.writeSunsetInfo(currentWeather: currentWeather)
            self.writeHumidityInfo(currentWeather: currentWeather)
            self.writeWindSpeedInfo(currentWeather: currentWeather)
            self.writeFeelsLikeInfo(currentWeather: currentWeather)
            self.writePressureInfo(currentWeather: currentWeather)
            self.writeVisibilityInfo(currentWeather: currentWeather)
            self.writeUviInfo(currentWeather: currentWeather)
        }
    }
}

//MARK: - Extensions

extension MainWeatherViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard (self.mainWeatherViewModel?.weather.value?.hourly) != nil else {
            return 0
        }
        return 25
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyWeatherCollectionViewCell", for: indexPath) as? HourlyWeatherCollectionViewCell,
              let hourlyWeather = self.mainWeatherViewModel?.weather.value?.hourly else {
            return UICollectionViewCell()
        }
        cell.config(with: hourlyWeather[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let hourlyWeatherCollectionViewBackground = self.hourlyWeatherCollectionViewBackground else {
            return CGSize()
        }
        let sideHeight: CGFloat = hourlyWeatherCollectionViewBackground.frame.height
        let sideWidth = hourlyWeatherCollectionViewBackground.frame.width / 5
        return CGSize(width: sideWidth, height: sideHeight)
    }
    
}

extension MainWeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let dailyWeather = self.mainWeatherViewModel?.weather.value?.daily else {
            return 0
        }
        return dailyWeather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeekWeatherTableViewCell", for: indexPath) as? WeekWeatherTableViewCell,
              let dailyWeather = self.mainWeatherViewModel?.weather.value?.daily else {
            return UITableViewCell()
        }
        cell.configure(object: dailyWeather[indexPath.row])
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let dailyWeather = self.mainWeatherViewModel?.weather.value?.daily,
              let dailyWeatherBackground = self.dailyWeatherTableViewBackground else {
            return 0
        }
        let cellHeight = dailyWeatherBackground.frame.height / CGFloat(dailyWeather.count)
        return cellHeight
    }
    
}



