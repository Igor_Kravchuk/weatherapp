import Foundation
import CoreLocation

//MARK: - ENUMS

class MainWeatherViewModel: NSObject, CLLocationManagerDelegate {
    //MARK: - IBOutlets
    
    //MARK: - VARS
    var location: CLLocationCoordinate2D?
    var weather = Bindable<Weather?>(nil)
    //MARK: - LETS
    let locationManager = CLLocationManager()
    let updateInterval = 0.1
    //MARK: - Life functions
    
    //MARK: - IBActions
    
    //MARK: - Flow functions
    func startUpdateLocation() {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]) {
        let location: CLLocationCoordinate2D = manager.location!.coordinate
        self.location = location
        self.loadWeatherForCurrentLocation()
    }
    
    func loadWeather() {
        self.loadWeatherForCurrentLocation()
    }
    
    func loadWeatherForCurrentLocation() {
        guard let location = self.location else {
            return
        }
        WeatherManager.shared.loadAPIWeather(latitude: location.latitude, longitude: location.longitude) { [weak self] (temporaryWeather) in
            self?.weather.value = temporaryWeather
        }
    }
    
    func writeCityName(value: String) -> String {
        if let slash = value.firstIndex(of: "/") {
            return String(value[value.index(after: slash)..<value.endIndex])
        }
        return ""
    }
}
