import UIKit
//MARK: - ENUMS

class WeekWeatherTableViewCell: UITableViewCell {
    //MARK: - IBOutlets
    @IBOutlet weak var dayNameLabel: UILabel?
    @IBOutlet weak var weatherIconImageView: UIImageView?
    @IBOutlet weak var maxDayTemperatureLabel: UILabel?
    @IBOutlet weak var minDayTemperatureLabel: UILabel?
    
    //MARK: - VARS
    
    //MARK: - LETS
    
    //MARK: - Flow functions
    func configure(object: Daily) {
        if let date = object.dt {
            let date = Date(timeIntervalSince1970: TimeInterval(date))
            let formatter = DateFormatter()
            formatter.dateFormat = "EEEE"
            let dayNameString = formatter.string(from: date)
            self.dayNameLabel?.text = dayNameString
        }
        
        if let minDayTemperature = object.temp?.min {
            self.minDayTemperatureLabel?.text = "\(minDayTemperature)°"
        }
        
        if let maxDayTemperature = object.temp?.max {
            self.maxDayTemperatureLabel?.text = "\(maxDayTemperature)°"
        }
        
        if let dayWeatherIcon = object.weather?[0].icon {
            self.weatherIconImageView?.image = UIImage(named: dayWeatherIcon)
        }
    }
}

