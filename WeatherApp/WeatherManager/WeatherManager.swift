import Foundation

class WeatherManager {
    static let shared = WeatherManager()
    private init(){}
    
    func loadAPIWeather(latitude: Double, longitude: Double, language: String = "ru", completionHandler: @escaping (_ result: Weather?) -> ()) {
        var weather: Weather?
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/onecall"
        urlComponents.queryItems = [
            URLQueryItem(name: "lat", value: "\(latitude)"),
            URLQueryItem(name: "lon", value: "\(longitude)"),
            URLQueryItem(name: "exclude", value: ""),
            URLQueryItem(name: "lang", value: "\(language)"),
            URLQueryItem(name: "units", value: "metric"),
            URLQueryItem(name: "appid", value: "26659a85bc29defbd92a2c67d5275949")
        ]
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            if error == nil, let data = data {
                do {
                    weather = try JSONDecoder().decode(Weather.self, from: data)
                    completionHandler(weather)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
}
