import Foundation

class Weather: Decodable {
    let lat: Double?
    let lon: Double?
    let timezone: String?
    let timezone_offset: Int?
    let current: Current?
    let minutely: [Minutely]?
    let hourly: [Hourly]?
    let daily: [Daily]?
}

class Current: Decodable {
    let dt: Int?
    let sunrise: Int?
    let sunset: Int?
    let temp: Double?
    let feels_like: Double?
    let pressure: Int?
    let humidity: Int?
    let dew_point: Double?
    let uvi: Double?
    let clouds: Int?
    let visibility: Int?
    let wind_speed: Double?
    let wind_deg: Int?
    let weather: [WeatherLocal]?
}

class WeatherLocal: Decodable {
    let id: Int?
    let main: String?
    let description: String?
    let icon: String?
}

class Minutely: Decodable {
    let dt: Int?
    let precipitation: Double?
}

class Hourly: Decodable {
    let dt: Int?
    let temp: Double?
    let feels_like: Double?
    let pressure: Int?
    let humidity: Int?
    let dew_point: Double?
    let uvi: Double?
    let clouds: Int?
    let visibility: Int?
    let wind_speed: Double?
    let wind_deg: Int?
    let weather: [WeatherLocal]?
    let pop: Double?
}

class Daily: Decodable {
    let dt: Int?
    let sunrise: Int?
    let sunset: Int?
    let temp: Temp?
    let feels_like: FeelsLike?
    let pressure: Int?
    let humidity: Int?
    let dew_point: Double?
    let wind_speed: Double?
    let wind_deg: Int?
    let weather: [WeatherLocal]?
    let clouds: Int?
    let pop: Double?
    let snow: Double?
    let uvi: Double?
}

class Temp: Decodable {
    let day: Double?
    let min: Double?
    let max: Double?
    let night: Double?
    let eve: Double?
    let morn: Double?
}

class FeelsLike: Decodable {
    let day: Double?
    let night: Double?
    let eve: Double?
    let morn: Double?
}
